package sk.pgl.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.pgl.spring.mail.MailSender;

import javax.mail.MessagingException;

// if using mailSender without name of resource == Dependency injection by NAME (of given field) == spring will try to find bean named MailSender
// but it is not possible (MailSender do not exists) == then spring do Dependency inject by type of this field == it will find
// MockMailSender because it is type of MailSender

@RestController
public class MailController {
    // private MailSender mailSender = new MockMailSender();    // NOT APPROPRIATE SOLUTION!

    //@Resource(name = "smtpMailSender")
    //@Resource
    private MailSender mailSender;

    @Autowired
    public MailController(/*@Qualifier("smtpMailSender") */MailSender mailSender) {
        this.mailSender = mailSender;
    }

    // Another possibility to inject dependency is to use setter method
    /*@Resource
    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }*/

    @RequestMapping("/mail")
    public String sendMail() throws MessagingException {
        mailSender.send("example@example.com", "Mail Subject", "Mail content");
        return "Mail sent";
    }
}
