package sk.pgl.spring.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @Value("${app.name}")
    private String appName;
    // It is also possible to set property values externally! - so with different environments it is possible to use different properties (configurations)

    @RequestMapping("/hello")
    public String hello() {
        return "HELLO " + appName;
    }
}
