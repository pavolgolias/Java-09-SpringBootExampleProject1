package sk.pgl.spring.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

// Example of how to use Java configuration class - in MockMailSender and SmtpMailSender - no annotations are included

@Configuration
public class MailConfig {

    // methods in Configuration class annotated with Bean - creates new instance only once and reuses already created one
    // we can use Component annotation, but Bean method will not be cached - so it will be created more than one time
    // it is good to put @Bean method to classes annotated with @Configuration
    @Bean
    public DemoBean demoBean() {
        return new DemoBean();
    }

    @Bean   // configuration annotated classes call methods annotated with Bean annotation
    //@Profile("dev")
    @ConditionalOnProperty(name = "spring.mail.host", havingValue = "foo", matchIfMissing = true)
    public MailSender mockMailSender() {    // the name of the method is the name of bean by default
        return new MockMailSender();
    }

    @Bean
    //@Profile("!dev")
    @ConditionalOnProperty(name = "spring.mail.host")
    public MailSender smtpMailSender(JavaMailSender javaMailSender/*, DemoBean demoBean*/) {
        /*demoBean.foo();*/
        /*demoBean().foo();*/
        SmtpMailSender smtpMailSender = new SmtpMailSender();
        smtpMailSender.setJavaMailSender(javaMailSender);
        return smtpMailSender;
    }
}
