package sk.pgl.spring.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

//@Component("abcMockMailSender")   // it is possible to change name of component like this, default for this class is "mockMailSender"
//@Component
public class MockMailSender implements MailSender {

    private static final Log log = LogFactory.getLog(MockMailSender.class);

    @Override
    public void send(String to, String subject, String body) {
        log.info("Sending mail to: " + to);
        log.info("Mail subject: " + subject);
        log.info("Mail body: " + body);
    }
}
